# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  max = arr.max
  min = arr.min
  max - min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = 'aeiouAEIOU'
  count = 0
  str.each_char do |i|
    letter = str[i]
    if vowels.include?(letter)
      count += 1
    end
  end
  count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  vowels = 'aeiouAEIOU'
  new_str = ""
  str.each_char do |i|
    letter = str[i]
    if vowels.include?(letter) == false
      new_str << letter
    end
  end
  new_str
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.split('').sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  new_str = str.downcase
  new_str2 = new_str.split('').uniq.join('')
  new_str != new_str2
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  arr = arr.join('')
  "(#{arr[0..2]}) #{arr[3..5]}-#{arr[6..9]}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  new_str = str.split(',')
  max = new_str.max.to_i
  min = new_str.min.to_i
  max - min
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  first_el = arr.take(offset % arr.length)
  rest = arr.drop(offset % arr.length)
  rest += first_el
end
